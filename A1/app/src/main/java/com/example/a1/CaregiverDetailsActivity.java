package com.example.a1;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.adapter.PatientRecyclerViewAdapter;
import com.example.a1.fragment.DatePickerFragment;
import com.example.a1.model.Caregiver;
import com.example.a1.model.Patient;
import com.example.a1.repository.CaregiverRepository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class CaregiverDetailsActivity extends AppCompatActivity {

    private static final String TAG = CaregiverDetailsActivity.class.getSimpleName();
    @BindView(R.id.data_name)
    TextView mName;

    @BindView(R.id.data_birth_date)
    TextView mBirthDate;

    @BindView(R.id.data_address)
    TextView mAddress;

    @BindView(R.id.data_gender)
    TextView mGender;

    @BindView(R.id.update_name)
    EditText dataName;

    @BindView(R.id.update_birth_date)
    Button dataBirthDate;

    @BindView(R.id.update_address)
    EditText dataAddress;

    @BindView(R.id.radioSex)
    RadioGroup dataGender;

    @BindView(R.id.patient_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.save_update)
    Button saveUpdate;

    @BindView(R.id.delete)
    Button delete;

    private Integer newId;

    final CaregiverRepository caregiverRepository = new CaregiverRepository();

    SingleObserver<Integer> createCaregiverObserver = new SingleObserver<Integer>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(Integer s) {
            newId = s;
            Log.d(TAG, "createPatientObserver onSuccess: " + s);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    private PatientRecyclerViewAdapter patientRecyclerViewAdapter;
    private List<Patient> mPatients = new ArrayList<>();

    SingleObserver<List<Patient>> allPatientsForCaregiverObserver = new SingleObserver<List<Patient>>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(List<Patient> patients) {
            mPatients.addAll(patients);
            patientRecyclerViewAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(Throwable e) {

        }
    };

    SingleObserver<String> updateObserver = new SingleObserver<String>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(String s) {
            Log.d(TAG, s);
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error on update: " + e);
        }
    };

    SingleObserver<Caregiver> caregiverObserver = new SingleObserver<Caregiver>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(final Caregiver caregiver) {
            mName.setText(caregiver.getName());
            mAddress.setText(caregiver.getAddress());
            mBirthDate.setText(caregiver.getBirthDate());
            mGender.setText(caregiver.isGender()? "Male" : "Female");

            caregiverRepository.getAllPatientsForCaregiver(caregiver.getId()).subscribe(allPatientsForCaregiverObserver);


            deleteCaregiver(caregiver.getId());

            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (saveUpdate.getText().equals("Update")) {
                        updateSetup();
                        dataAddress.setText(mAddress.getText().toString());
                        dataName.setText(mName.getText().toString());
                        if (caregiver.isGender()) dataGender.check(R.id.radioMale);
                        else dataGender.check(R.id.radioFemale);
                    }
                    else {
                        saveSetup(caregiver.getId(), caregiver.getUserName());
                    }
                }
            });
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    SingleObserver<String> deleteObserver = new SingleObserver<String>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(String s) {
            Log.d(TAG, "success on delete");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error on delete: "+ e);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caregiver_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        int id = intent.getIntExtra("car", -1);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        patientRecyclerViewAdapter = new PatientRecyclerViewAdapter(mPatients);
        recyclerView.setAdapter(patientRecyclerViewAdapter);

        if (id == 0) {
            delete.setVisibility(View.INVISIBLE);
            updateSetup();

            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete.setVisibility(View.VISIBLE);
                    saveSetup(0, "");
                }
            });

            deleteCaregiver(newId);
        }
        else caregiverRepository.getCaregiver(id).subscribe(caregiverObserver);
    }

    public void updateSetup() {
        mName.setVisibility(View.INVISIBLE);
        mBirthDate.setVisibility(View.INVISIBLE);
        mAddress.setVisibility(View.INVISIBLE);
        mGender.setVisibility(View.INVISIBLE);
        dataName.setVisibility(View.VISIBLE);
        dataBirthDate.setVisibility(View.VISIBLE);
        dataAddress.setVisibility(View.VISIBLE);
        dataGender.setVisibility(View.VISIBLE);
        dataBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        saveUpdate.setText(R.string.save);
    }

    public void saveSetup(Integer id, String userName) {
        String name = dataName.getText().toString();
        String address = dataAddress.getText().toString();
        int selected = dataGender.getCheckedRadioButtonId();
        String gender = ((RadioButton) findViewById(selected)).getText().toString();
        String birthDate = mBirthDate.getText().toString();
        Caregiver m = new Caregiver(id, name, address, birthDate, gender.equals("Male"), userName);
        if (id == 0) caregiverRepository.createCaregiver(m).subscribe(createCaregiverObserver);
        else caregiverRepository.updateCaregiver(m).subscribe(updateObserver);
        mName.setVisibility(View.VISIBLE);
        mBirthDate.setVisibility(View.VISIBLE);
        mAddress.setVisibility(View.VISIBLE);
        mGender.setVisibility(View.VISIBLE);
        dataName.setVisibility(View.INVISIBLE);
        dataAddress.setVisibility(View.INVISIBLE);
        dataBirthDate.setVisibility(View.INVISIBLE);
        dataAddress.setVisibility(View.INVISIBLE);
        dataGender.setVisibility(View.INVISIBLE);
        mName.setText(name);
        mGender.setText(gender);
        mAddress.setText(address);
        //mBirthDate.setText(birthDate);
        saveUpdate.setText(R.string.update);
    }

    public void deleteCaregiver(final Integer idDelete) {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                caregiverRepository.deleteCaregiver(idDelete).subscribe(deleteObserver);
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
