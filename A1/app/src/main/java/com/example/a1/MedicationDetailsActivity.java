package com.example.a1;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.a1.model.Medication;
import com.example.a1.repository.MedicationRepository;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class MedicationDetailsActivity extends AppCompatActivity {

    private static final String TAG = MedicationDetailsActivity.class.getSimpleName();
    @BindView(R.id.data_name)
    TextView mName;

    @BindView(R.id.data_dosage)
    TextView mDosage;

    @BindView(R.id.data_side_effect)
    TextView mSideEffect;

    @BindView(R.id.update_name)
    EditText dataName;

    @BindView(R.id.update_dosage)
    EditText dataDosage;

    @BindView(R.id.update_side_effect)
    EditText dataSideEffect;

    @BindView(R.id.save_update)
    Button saveUpdate;

    @BindView(R.id.delete)
    Button delete;

    private Integer newId;

    final MedicationRepository medicationRepository = new MedicationRepository();

    SingleObserver<Integer> createMedicationObserver = new SingleObserver<Integer>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(Integer s) {
            newId = s;
            Log.d(TAG, "createPatientObserver onSuccess: " + s);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    SingleObserver<Medication> medicationObserver = new SingleObserver<Medication>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(final Medication medication) {
            Log.d(TAG, medication.getName() + " " + medication.getSideEffect() + " " + medication.getDosage());
            mName.setText(medication.getName());
            mDosage.setText(String.valueOf(medication.getDosage()));
            mSideEffect.setText(medication.getSideEffect());

            deleteMedication(medication.getId());

            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (saveUpdate.getText().equals("Update")) {
                        saveSetup();
                    } else {
                        updateSetup(medication.getId());
                    }
                }
            });
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        int id = intent.getIntExtra("med", -1);

        if (id == 0) {
            delete.setVisibility(View.INVISIBLE);
            saveSetup();
            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateSetup(0);
                    delete.setVisibility(View.VISIBLE);
                }
            });

            deleteMedication(newId);
        } else medicationRepository.getMedication(id).subscribe(medicationObserver);
    }

    public void saveSetup() {
        mName.setVisibility(View.INVISIBLE);
        mDosage.setVisibility(View.INVISIBLE);
        mSideEffect.setVisibility(View.INVISIBLE);
        dataName.setVisibility(View.VISIBLE);
        dataDosage.setVisibility(View.VISIBLE);
        dataSideEffect.setVisibility(View.VISIBLE);
        dataName.setText(mName.getText().toString());
        dataDosage.setText(mDosage.getText().toString());
        dataSideEffect.setText(mSideEffect.getText().toString());
        saveUpdate.setText(R.string.save);
    }

    public void updateSetup(Integer id) {
        String name = dataName.getText().toString();
        String dosage = dataDosage.getText().toString();
        String sideEffect = dataSideEffect.getText().toString();
        Medication m = new Medication(id, name, sideEffect, Integer.valueOf(dosage));
        if (id == 0) medicationRepository.createMedication(m).subscribe(createMedicationObserver);
        else medicationRepository.updateMedication(m).subscribe();
        mName.setVisibility(View.VISIBLE);
        mDosage.setVisibility(View.VISIBLE);
        mSideEffect.setVisibility(View.VISIBLE);
        dataName.setVisibility(View.INVISIBLE);
        dataDosage.setVisibility(View.INVISIBLE);
        dataSideEffect.setVisibility(View.INVISIBLE);
        mName.setText(name);
        mDosage.setText(dosage);
        mSideEffect.setText(sideEffect);
        saveUpdate.setText(R.string.update);
    }

    public void deleteMedication(final Integer id) {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medicationRepository.deleteMedication(id).subscribe();
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
