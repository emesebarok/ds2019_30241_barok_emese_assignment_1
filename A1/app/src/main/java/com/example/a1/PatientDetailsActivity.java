package com.example.a1;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.example.a1.fragment.DatePickerFragment;
import com.example.a1.model.Caregiver;
import com.example.a1.model.Patient;
import com.example.a1.repository.CaregiverRepository;
import com.example.a1.repository.PatientRepository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class PatientDetailsActivity extends AppCompatActivity{

    private static final String TAG = PatientDetailsActivity.class.getSimpleName();
    @BindView(R.id.data_name)
    TextView mName;

    @BindView(R.id.data_birth_date)
    TextView mBirthDate;

    @BindView(R.id.data_address)
    TextView mAddress;

    @BindView(R.id.data_gender)
    TextView mGender;

    @BindView(R.id.data_medical_record)
    TextView mMedicalRecord;

    @BindView(R.id.update_name)
    EditText updateName;

    @BindView(R.id.update_birth_date)
    Button updateBirthDate;

    @BindView(R.id.update_address)
    EditText updateAddress;

    @BindView(R.id.radioSex)
    RadioGroup updateGender;

    @BindView(R.id.update_medical_record)
    EditText updateMedicalRecord;

    @BindView(R.id.choose_caregiver)
    Button chooseCaregiver;

    @BindView(R.id.save_update)
    Button saveUpdate;

    @BindView(R.id.delete)
    Button delete;

    private Integer newId;

    private Activity mActivity;

    final PatientRepository patientRepository = new PatientRepository();
    private Caregiver selectedCaregiver;

    private String mUserName;

    SingleObserver<Integer> createPatientObserver = new SingleObserver<Integer>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(Integer s) {
            newId = s;
            Log.d(TAG, "createPatientObserver onSuccess: " + s);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    SingleObserver<Caregiver> caregiverSingleObserver = new SingleObserver<Caregiver>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(Caregiver caregiver) {
            selectedCaregiver = caregiver;
        }

        @Override
        public void onError(Throwable e) {

        }
    };

    SingleObserver<String> updateObserver = new SingleObserver<String>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(String s) {
            Log.d(TAG, s);
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error on update: " + e);
        }
    };

    SingleObserver<List<Caregiver>> caregiverListObserver = new SingleObserver<List<Caregiver>>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(final List<Caregiver> caregivers) {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.radiobutton_group);
            RadioGroup rg = dialog.findViewById(R.id.radio_group);
            for (Caregiver c : caregivers) {
                RadioButton rb = new RadioButton(mActivity); // dynamically creating RadioButton and adding to RadioGroup.
                rb.setText(c.getName());
                rg.addView(rb);
            }
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    selectedCaregiver = caregivers.get(i - 1);
                    Log.d(TAG, "selected");
                }
            });
            dialog.show();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "caregiver list error: " + e);
        }
    };

    SingleObserver<Patient> patientSingleObserver = new SingleObserver<Patient>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(final Patient patient) {
            mName.setText(patient.getName());
            mAddress.setText(patient.getAddress());
            mBirthDate.setText(patient.getBirthDate());
            mGender.setText(patient.isGender() ? "Male" : "Female");
            mMedicalRecord.setText(patient.getMedicalRecord());
            patientRepository.getCaregiverForPatient(patient.getId()).subscribe(caregiverSingleObserver);

            deleteItem(patient.getId());

            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (saveUpdate.getText().equals("Update")) {
                        saveVisibility();
                        updateName.setText(mName.getText().toString());
                        updateAddress.setText(mAddress.getText().toString());
                        if (patient.isGender()) updateGender.check(R.id.radioMale);
                        else updateGender.check(R.id.radioFemale);
                        updateMedicalRecord.setText(mMedicalRecord.getText().toString());
                        saveUpdate.setText(R.string.save);
                    } else {
                        updateVisibility(patient.getId(), patient.getUserName());
                    }
                }
            });
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "error: " + e);
        }
    };

    SingleObserver<String> deleteObserver = new SingleObserver<String>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(String s) {
            Log.d(TAG, "onSuccess");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error: " + e);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        ButterKnife.bind(this);
        mActivity = this;

        getSharedPreferences();

        if (mUserName.startsWith("caregiver")) {
            saveUpdate.setVisibility(View.INVISIBLE);
            delete.setVisibility(View.INVISIBLE);
        }

        Intent intent = getIntent();
        int id = intent.getIntExtra("pat", -1);

        if (id == 0) {
            delete.setVisibility(View.INVISIBLE);
            saveVisibility();
            saveUpdate.setText(R.string.save);
            saveUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete.setVisibility(View.VISIBLE);
                    updateVisibility(0, "");
                }
            });

            deleteItem(newId);
        } else patientRepository.getPatient(id).subscribe(patientSingleObserver);
    }

    public void saveVisibility() {
        mName.setVisibility(View.INVISIBLE);
        mBirthDate.setVisibility(View.INVISIBLE);
        mAddress.setVisibility(View.INVISIBLE);
        mGender.setVisibility(View.INVISIBLE);
        mMedicalRecord.setVisibility(View.INVISIBLE);
        updateName.setVisibility(View.VISIBLE);
        updateBirthDate.setVisibility(View.VISIBLE);
        updateAddress.setVisibility(View.VISIBLE);
        updateGender.setVisibility(View.VISIBLE);
        updateMedicalRecord.setVisibility(View.VISIBLE);
        chooseCaregiver.setVisibility(View.VISIBLE);
        updateBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
        chooseCaregiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CaregiverRepository caregiverRepository = new CaregiverRepository();
                caregiverRepository.getCaregivers().subscribe(caregiverListObserver);
            }
        });
    }

    public void updateVisibility(Integer id, String userName) {
        String name = updateName.getText().toString();
        String address = updateAddress.getText().toString();
        int selected = updateGender.getCheckedRadioButtonId();
        String gender = ((RadioButton) findViewById(selected)).getText().toString();
        String birthDate = mBirthDate.getText().toString();
        String medicalRecord = updateMedicalRecord.getText().toString();
        Patient m = new Patient(id, name, address, birthDate, gender.equals("Male"), medicalRecord, userName, selectedCaregiver);
        if (id == 0) patientRepository.createPatient(m).subscribe(createPatientObserver);
        else patientRepository.updatePatient(m).subscribe(updateObserver);
        mName.setVisibility(View.VISIBLE);
        mBirthDate.setVisibility(View.VISIBLE);
        mAddress.setVisibility(View.VISIBLE);
        mGender.setVisibility(View.VISIBLE);
        mMedicalRecord.setVisibility(View.VISIBLE);
        updateName.setVisibility(View.INVISIBLE);
        updateAddress.setVisibility(View.INVISIBLE);
        updateBirthDate.setVisibility(View.INVISIBLE);
        updateAddress.setVisibility(View.INVISIBLE);
        chooseCaregiver.setVisibility(View.INVISIBLE);
        updateGender.setVisibility(View.INVISIBLE);
        updateMedicalRecord.setVisibility(View.INVISIBLE);
        mName.setText(name);
        mGender.setText(gender);
        mAddress.setText(address);
        mMedicalRecord.setText(medicalRecord);
        //mBirthDate.setText(birthDate);
        saveUpdate.setText(R.string.update);
    }

    public void deleteItem(final Integer idDelete) {
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                patientRepository.deletePatient(idDelete).subscribe(deleteObserver);
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getSharedPreferences() {
        SharedPreferences userDetails = this.getSharedPreferences("userdetails", MODE_PRIVATE);
        mUserName = userDetails.getString("username", "");
    }
}
