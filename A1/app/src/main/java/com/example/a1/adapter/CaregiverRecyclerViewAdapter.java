package com.example.a1.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.CaregiverDetailsActivity;
import com.example.a1.R;
import com.example.a1.model.Caregiver;


import java.util.List;

public class CaregiverRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = CaregiverRecyclerViewAdapter.class.getSimpleName();

    private List<Caregiver> caregiverList;

    public CaregiverRecyclerViewAdapter(List<Caregiver> caregiverList) {
        this.caregiverList = caregiverList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new BaseViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, final int position) {
        final String s = caregiverList.get(position).getName();
        final Integer i = caregiverList.get(position).getId();
        holder.textView.setText(s);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CaregiverDetailsActivity.class);
                intent.putExtra("car", i);
                view.getContext().startActivity(intent);
                //Navigation.findNavController(holder.textView).navigate(R.id.caregiverListFragment_to_caregiverDetailsFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return caregiverList.size();
    }
}
