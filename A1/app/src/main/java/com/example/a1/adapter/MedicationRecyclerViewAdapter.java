package com.example.a1.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.MedicationDetailsActivity;
import com.example.a1.R;
import com.example.a1.model.Medication;

import java.util.List;

public class MedicationRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = MedicationRecyclerViewAdapter.class.getSimpleName();

    private List<Medication> medicationArrayList;

    public MedicationRecyclerViewAdapter(List<Medication> medicationArrayList) {
        this.medicationArrayList = medicationArrayList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new BaseViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, final int position) {
        final String s = medicationArrayList.get(position).getName();
        final Integer i = medicationArrayList.get(position).getId();
        Log.d(TAG, medicationArrayList.size()+"");
        Log.d(TAG, s);
        holder.textView.setText(s);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MedicationDetailsActivity.class);
                intent.putExtra("med", i);
                view.getContext().startActivity(intent);
                //Navigation.findNavController(holder.textView).navigate(R.id.medicationListFragment_to_medicationDetailsFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return medicationArrayList.size();
    }
}
