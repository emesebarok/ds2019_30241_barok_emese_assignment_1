package com.example.a1.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.PatientDetailsActivity;
import com.example.a1.R;
import com.example.a1.model.Patient;

import java.util.List;

public class PatientRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = PatientRecyclerViewAdapter.class.getSimpleName();

    private List<Patient> patientList;

    public PatientRecyclerViewAdapter(List<Patient> patientList) {
        this.patientList = patientList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new BaseViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, final int position) {
        final String s = patientList.get(position).getName();
        final Integer i = patientList.get(position).getId();
        holder.textView.setText(s);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PatientDetailsActivity.class);
                intent.putExtra("pat", i);
                view.getContext().startActivity(intent);
                //Navigation.findNavController(holder.textView).navigate(R.id.patientListFragment_to_patientDetailsFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return patientList.size();
    }
}