package com.example.a1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.CaregiverDetailsActivity;
import com.example.a1.R;
import com.example.a1.adapter.CaregiverRecyclerViewAdapter;
import com.example.a1.model.Caregiver;
import com.example.a1.repository.CaregiverRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class CaregiverListFragment extends Fragment {
    private static final String TAG = CaregiverListFragment.class.getSimpleName();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    private List<Caregiver> mCaregivers = new ArrayList<>();

    private View mView;
    private CaregiverRecyclerViewAdapter caregiverRecyclerViewAdapter;

    private SingleObserver<List<Caregiver>> observer = new SingleObserver<List<Caregiver>>() {
        @Override
        public void onSubscribe(Disposable d) {
            Log.d(TAG, "onSubscribe");
        }

        @Override
        public void onSuccess(List<Caregiver> caregivers) {
            mCaregivers.addAll(caregivers);
            caregiverRecyclerViewAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error: " + e);
        }
    };

    public CaregiverListFragment () {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        caregiverRecyclerViewAdapter = new CaregiverRecyclerViewAdapter(mCaregivers);
        recyclerView.setAdapter(caregiverRecyclerViewAdapter);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CaregiverRepository caregiverRepository = new CaregiverRepository();
        caregiverRepository.getCaregivers().subscribe(observer);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CaregiverDetailsActivity.class);
                intent.putExtra("car", 0);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
