package com.example.a1.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.a1.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class ChooseFragment extends Fragment {

    private static final String TAG = ChooseFragment.class.getSimpleName();

    @BindView(R.id.caregiver_button)
    Button caregiverButton;

    @BindView(R.id.patient_button)
    Button patientButton;

    @BindView(R.id.medication_button)
    Button medicationButton;

    private String mUserName;

    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_choose, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getSharedPreferences();

        if (mUserName.startsWith("doctor")) {
            caregiverButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Navigation.findNavController(mView).navigate(R.id.chooseFragment_to_caregiverListFragment);
                }
            });

            patientButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Navigation.findNavController(mView).navigate(R.id.chooseFragment_to_patientListFragment);
                }
            });

            medicationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Navigation.findNavController(mView).navigate(R.id.chooseFragment_to_medicationListFragment);
                }
            });
        }
        else if (mUserName.startsWith("caregiver")) {
            caregiverButton.setVisibility(View.INVISIBLE);
            medicationButton.setVisibility(View.INVISIBLE);
            patientButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Navigation.findNavController(mView).navigate(R.id.chooseFragment_to_patientListFragment);
                }
            });
        }
        else {
            Log.d(TAG, "here");
            caregiverButton.setVisibility(View.INVISIBLE);
            medicationButton.setVisibility(View.INVISIBLE);
            patientButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getSharedPreferences() {
        SharedPreferences userDetails = getContext().getSharedPreferences("userdetails", MODE_PRIVATE);
        mUserName = userDetails.getString("username", "");
    }
}
