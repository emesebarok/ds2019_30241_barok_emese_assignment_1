package com.example.a1.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.a1.R;
import com.example.a1.repository.CaregiverRepository;
import com.example.a1.repository.DoctorRepository;
import com.example.a1.repository.PatientRepository;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends Fragment {
    @BindView(R.id.login_button)
    Button loginButton;

    @BindView(R.id.login_user_name)
    EditText loginUserName;

    private View mView;

    private SingleObserver<Boolean> observer = new SingleObserver<Boolean>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(Boolean aBoolean) {
            getSharedPreferences(0);
            if (aBoolean) Navigation.findNavController(mView).navigate(R.id.loginFragment_to_chooseFragment);
            else Toast.makeText(getContext(), "Username does not exist!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(Throwable e) {

            Toast.makeText(getContext(), "Error: " + e, Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userName = loginUserName.getText().toString().trim();

                if (userName.startsWith("doctor")) {
                    new DoctorRepository().itExists(userName).subscribe(observer);
                }
                else if (userName.startsWith("caregiver")) {
                    new CaregiverRepository().itExists(userName).subscribe(observer);
                }
                else new PatientRepository().itExists(userName).subscribe(observer);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getSharedPreferences(Integer id) {
        SharedPreferences userDetails = getContext().getSharedPreferences("userdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = userDetails.edit();
        edit.putString("username", loginUserName.getText().toString().trim());
        edit.putInt("userID", id);
        edit.apply();
    }
}
