package com.example.a1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.MedicationDetailsActivity;
import com.example.a1.R;
import com.example.a1.adapter.MedicationRecyclerViewAdapter;
import com.example.a1.model.Medication;
import com.example.a1.repository.MedicationRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class MedicationListFragment extends Fragment {
    private static final String TAG = MedicationListFragment.class.getSimpleName();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    private List<Medication> mMedications = new ArrayList<>();

    private View mView;
    private MedicationRecyclerViewAdapter medicationRecyclerViewAdapter;

    private SingleObserver<List<Medication>> observer = new SingleObserver<List<Medication>>() {
        @Override
        public void onSubscribe(Disposable d) {
            Log.d(TAG, "onSubscribe");
        }

        @Override
        public void onSuccess(List<Medication> medications) {
            Log.d(TAG, "onSuccess: " + medications.size());
            for (Medication m : medications) {
                Log.d(TAG, m.getName());
                mMedications.add(m);
            }
//            RecyclerView recyclerView = mView.findViewById(R.id.recyclerview);
//            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            medicationRecyclerViewAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error: " + e);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        medicationRecyclerViewAdapter = new MedicationRecyclerViewAdapter(mMedications);
        recyclerView.setAdapter(medicationRecyclerViewAdapter);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MedicationRepository medicationRepository = new MedicationRepository();
        medicationRepository.getMedications().subscribe(observer);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), MedicationDetailsActivity.class);
                intent.putExtra("med", 0);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
