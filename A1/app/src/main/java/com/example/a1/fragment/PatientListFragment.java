package com.example.a1.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a1.PatientDetailsActivity;
import com.example.a1.R;
import com.example.a1.adapter.PatientRecyclerViewAdapter;
import com.example.a1.model.Patient;
import com.example.a1.repository.PatientRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static android.content.Context.MODE_PRIVATE;

public class PatientListFragment extends Fragment {

    private static final String TAG = PatientListFragment.class.getSimpleName();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    private List<Patient> mPatients = new ArrayList<>();

    private String mUserName;
    private View mView;
    private PatientRecyclerViewAdapter patientRecyclerViewAdapter;

    private SingleObserver<List<Patient>> observer = new SingleObserver<List<Patient>>() {
        @Override
        public void onSubscribe(Disposable d) {
            Log.d(TAG, "onSubscribe");
        }

        @Override
        public void onSuccess(List<Patient> patients) {
            mPatients.addAll(patients);
            patientRecyclerViewAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "error: " + e);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        patientRecyclerViewAdapter = new PatientRecyclerViewAdapter(mPatients);
        recyclerView.setAdapter(patientRecyclerViewAdapter);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PatientRepository patientRepository = new PatientRepository();
        patientRepository.getPatients().subscribe(observer);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PatientDetailsActivity.class);
                intent.putExtra("pat", 0);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getSharedPreferences() {
        SharedPreferences userDetails = getContext().getSharedPreferences("userdetails", MODE_PRIVATE);
        mUserName = userDetails.getString("username", "");
    }
}
