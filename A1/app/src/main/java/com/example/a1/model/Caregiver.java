package com.example.a1.model;

import com.example.a1.model.Patient;
import com.google.gson.annotations.SerializedName;

import java.sql.Date;
import java.util.List;

public class Caregiver extends Person {

    @SerializedName("patients")
    private List<Patient> patients;

    public Caregiver(Integer id, String name, String address, String  birthDate, boolean gender, String userName) {
        super(id, name, address, birthDate, gender, userName);
    }

    public Caregiver(Integer id, String name, String address, String  birthDate, boolean gender, String userName, List<Patient> patients) {
        super(id, name, address, birthDate, gender, userName);
        this.patients = patients;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
