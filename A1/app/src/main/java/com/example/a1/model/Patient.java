package com.example.a1.model;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

public class Patient extends Person {

    @SerializedName("medicalRecord")
    private String medicalRecord;

    @SerializedName("caregiver")
    private Caregiver caregiver;

    public Patient(Integer id, String name, String address, String birthDate, boolean gender, String medicalRecord, String userName) {
        super(id, name, address, birthDate, gender, userName);
        this.medicalRecord = medicalRecord;
    }

    public Patient(Integer id, String name, String address, String birthDate, boolean gender, String medicalRecord, String userName, Caregiver caregiver) {
        super(id, name, address, birthDate, gender, userName);
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
