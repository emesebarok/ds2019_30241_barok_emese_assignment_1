package com.example.a1.repository;

import com.example.a1.model.Caregiver;
import com.example.a1.model.Patient;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CaregiverRepository {
    private GetData getData;

    public CaregiverRepository() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        getData = retrofitInstance.getRetrofit().create(GetData.class);
    }

    public Single<Integer> createCaregiver(Caregiver caregiver) {
        return getData.createCaregiver(caregiver).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Caregiver>> getCaregivers() {
        return getData.getAllCaregivers().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Caregiver> getCaregiver(Integer id) {
        return getData.getCaregiver(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Patient>> getAllPatientsForCaregiver(Integer id) {
        return getData.getAllPatientsForCaregiver(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Boolean> itExists(String userName) {
        return getData.doesCaregiverExist(userName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> updateCaregiver(Caregiver caregiver) {
        return getData.updateCaregiver(caregiver).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> deleteCaregiver(Integer caregiver){
        return getData.deleteCaregiver(caregiver).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
