package com.example.a1.repository;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DoctorRepository {

    private GetData getData;

    public DoctorRepository() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        getData = retrofitInstance.getRetrofit().create(GetData.class);
    }

    public Single<Boolean> itExists(String userName) {
        return getData.doesDoctorExist(userName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
