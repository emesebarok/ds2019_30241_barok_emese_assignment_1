package com.example.a1.repository;

import com.example.a1.model.Caregiver;
import com.example.a1.model.Medication;
import com.example.a1.model.Patient;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface GetData {

    //create patient
    @POST("/patient")
    Single<Integer> createPatient(@Body Patient patient); //without id!!!

    //read patients
    @GET("/patient")
    Single<List<Patient>> getAllPatients();

    @GET("/patient/{id}")
    Single<Patient> getPatient(@Path("id") Integer id);

    @GET("/patient/{id}/caregiver")
    Single<Caregiver> getCaregiverForPatient(@Path("id") Integer id);

    @GET("/patient/username/{username}")
    Single<Boolean> doesPatientExist(@Path("username") String userName);

    //update patient
    @PUT("/patient")
    Single<String> updatePatient(@Body Patient patient);

    //delete patient
    @DELETE("/patients/{id}")
    Single<String> deletePatient(@Path("id") Integer id);

    //create caregiver
    @POST("/caregiver")
    Single<Integer> createCaregiver(@Body Caregiver caregiver);

    //read all caregivers
    @GET("/caregiver")
    Single<List<Caregiver>> getAllCaregivers();

    @GET("/caregiver/{id}")
    Single<Caregiver> getCaregiver(@Path("id") Integer id);

    @GET("/caregiver/{id}/patients")
    Single<List<Patient>> getAllPatientsForCaregiver(@Path("id") Integer id);

    @GET("/caregiver/username/{username}")
    Single<Boolean> doesCaregiverExist(@Path("username") String userName);

    //update caregiver
    @PUT("/caregiver")
    Single<String> updateCaregiver(@Body Caregiver caregiver);

    //delete caregiver
    @DELETE("/caregivers/{id}")
    Single<String> deleteCaregiver(@Path("id") Integer id);

    //create medication
    @POST("/medication")
    Single<Integer> createMedication(@Body Medication medication);

    //read all medications
    @GET("/medication")
    Single<List<Medication>> getAllMedications();

    @GET("/medication/{id}")
    Single<Medication> getMedication(@Path("id") Integer id);

    //update medication
    @PUT("/medication")
    Single<String> updateMedication(@Body Medication medication);

    //delete medication
    @DELETE("/medications/{id}")
    Single<String> deleteMedication(@Path("id") Integer id);

    @GET("/doctor/username/{username}")
    Single<Boolean> doesDoctorExist(@Path("username") String userName);
}
