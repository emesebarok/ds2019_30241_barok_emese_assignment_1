package com.example.a1.repository;

import com.example.a1.model.Medication;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MedicationRepository {

    private GetData getData;

    public MedicationRepository() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        getData = retrofitInstance.getRetrofit().create(GetData.class);
    }

    public Single<Integer> createMedication(Medication medication) {
        return getData.createMedication(medication).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Medication>> getMedications() {
        return getData.getAllMedications().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Medication> getMedication(Integer id) {
        return getData.getMedication(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> updateMedication(Medication medication) {
        return getData.updateMedication(medication).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> deleteMedication(Integer id){
        return getData.deleteMedication(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
