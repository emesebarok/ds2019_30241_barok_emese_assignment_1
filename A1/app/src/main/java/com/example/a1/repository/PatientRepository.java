package com.example.a1.repository;

import com.example.a1.model.Caregiver;
import com.example.a1.model.Patient;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PatientRepository {
    private GetData getData;

    public PatientRepository() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        getData = retrofitInstance.getRetrofit().create(GetData.class);
    }

    public Single<Integer> createPatient(Patient patient) {
        return getData.createPatient(patient).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Patient>> getPatients() {
        return getData.getAllPatients().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Patient> getPatient(Integer id) {
        return getData.getPatient(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Caregiver> getCaregiverForPatient(Integer id) {
        return getData.getCaregiverForPatient(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Boolean> itExists(String userName) {
        return getData.doesPatientExist(userName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> updatePatient(Patient patient) {
        return getData.updatePatient(patient).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<String> deletePatient(Integer patient){
        return getData.deletePatient(patient).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
