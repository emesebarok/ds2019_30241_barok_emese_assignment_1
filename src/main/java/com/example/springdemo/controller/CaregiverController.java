package com.example.springdemo.controller;


import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public CaregiverViewDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findCaregiverById(id);
    }

    @GetMapping()
    public List<CaregiverViewDTO> findAll(){
        return caregiverService.findAll();
    }

    @GetMapping(value = "/{id}/patients")
    public List<PatientDTO> findAllPatients(@PathVariable("id") Integer id){
        return caregiverService.findAllFetch(id);
    }

    @GetMapping(value = "/username/{username}")
    public boolean itExists(@PathVariable("username") String userName) {
        return caregiverService.itExists(userName) != null;
    }

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insert(caregiverDTO);
    }

    @PutMapping()
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody CaregiverViewDTO caregiverViewDTO){
        caregiverService.delete(caregiverViewDTO);
    }
}
