package com.example.springdemo.controller;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {
    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/{id}")
    public DoctorViewDTO findById(@PathVariable("id") Integer id){
        return doctorService.findDoctorById(id);
    }

    @GetMapping()
    public List<DoctorViewDTO> findAll(){
        return doctorService.findAll();
    }

    @GetMapping(value = "/username/{username}")
    public boolean itExists(@PathVariable("username") String userName) {
        return doctorService.itExists(userName) != null;
    }

    @PostMapping()
    public Integer insertDoctorDTO(@RequestBody DoctorDTO doctorDTO){
        return doctorService.insert(doctorDTO);
    }

    @PutMapping()
    public Integer updateDoctor(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.update(doctorDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody DoctorViewDTO doctorViewDTO){
        doctorService.delete(doctorViewDTO);
    }
}
