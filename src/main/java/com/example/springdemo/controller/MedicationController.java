package com.example.springdemo.controller;

import com.example.springdemo.dto.*;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationViewDTO findById(@PathVariable("id") Integer id){
        return medicationService.findMedicationById(id);
    }

    @GetMapping()
    public List<MedicationViewDTO> findAll(){
        return medicationService.findAll();
    }

    @PostMapping()
    public Integer insertMedicationDTO(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insert(medicationDTO);
    }

    @PutMapping()
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody MedicationViewDTO medicationViewDTO){
        medicationService.delete(medicationViewDTO);
    }
}
