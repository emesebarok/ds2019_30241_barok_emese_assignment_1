package com.example.springdemo.controller;

import com.example.springdemo.dto.*;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    private PatientService patientService;

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping()
    public List<PatientViewDTO> findAll(){
        return patientService.findAll();
    }

    @GetMapping(value = "{id}/caregiver")
    public Caregiver findWithItems(@PathVariable("id") Integer id){
        return patientService.findFetch(id);
    }

    @GetMapping(value = "/username/{username}")
    public boolean itExists(@PathVariable("username") String userName) {
        return patientService.itExists(userName) != null;
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientWithCaregiverDTO patientWithCaregiverDTO){
        return patientService.insert(patientWithCaregiverDTO);
    }

    @PutMapping()
    public Integer updatePatient(@RequestBody PatientWithCaregiverDTO patientWithCaregiverDTO) {
        return patientService.update(patientWithCaregiverDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }
}
