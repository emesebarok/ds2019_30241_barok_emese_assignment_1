package com.example.springdemo.dto;

import java.sql.Date;
import java.util.List;

public class CaregiverWithPatientsDTO {
    private Integer id;
    private String name;
    private Date birthDate;
    private boolean gender;
    private String address;
    private List<PatientDTO> patientDTOS;

    public CaregiverWithPatientsDTO(){}

    public CaregiverWithPatientsDTO(Integer id, String name, Date birthDate, boolean gender, String address, List<PatientDTO> patientDTOS) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patientDTOS = patientDTOS;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientDTO> getPatientDTOS() {
        return patientDTOS;
    }

    public void setPatientDTOS(List<PatientDTO> patientDTOS) {
        this.patientDTOS = patientDTOS;
    }

}
