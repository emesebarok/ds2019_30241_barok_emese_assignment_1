package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

public class DoctorDTO {

    private Integer id;
    private String name;
    private Date birthDate;
    private boolean gender;
    private String address;
    private String userName;

    public DoctorDTO(Integer id, String name, Date birthDate, boolean gender, String address, String userName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(id, doctorDTO.id) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(birthDate, doctorDTO.birthDate) &&
                Objects.equals(gender, doctorDTO.gender) &&
                Objects.equals(address, doctorDTO.address) &&
                Objects.equals(userName, doctorDTO.userName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, birthDate, gender, address, userName);
    }
}
