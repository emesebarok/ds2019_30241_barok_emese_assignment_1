package com.example.springdemo.dto;

public class MedicationDTO {
    private Integer id;
    private String name;
    private String sideEffect;
    private Integer dosage;

    public MedicationDTO(Integer id, String name, String sideEffect, Integer dosage) {
        this.id = id;
        this.name = name;
        this.sideEffect = sideEffect;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
