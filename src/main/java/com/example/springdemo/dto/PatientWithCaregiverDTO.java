package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;

import java.sql.Date;

public class PatientWithCaregiverDTO {
    private Integer id;
    private String name;
    private Date birthDate;
    private boolean gender;
    private String address;
    private String medicalRecord;
    private String userName;
    private Caregiver caregiver;

    public PatientWithCaregiverDTO() {
    }

    public PatientWithCaregiverDTO(Integer id, String name, Date birthDate, boolean gender, String address, String medicalRecord, String userName, Caregiver caregiver) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.userName = userName;
        this.caregiver = caregiver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
