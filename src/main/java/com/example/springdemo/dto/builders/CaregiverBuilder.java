package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.isGender(),
                caregiver.getAddress(),
                caregiver.getUserName());
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO, Integer i){
        String userName;
        if (i == 0) userName = "caregiver" + caregiverDTO.getId();
        else userName = "caregiver" + i;
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.isGender(),
                caregiverDTO.getAddress(),
                userName);
    }
}
