package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverViewBuilder {
    private CaregiverViewBuilder(){}

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.isGender(),
                caregiver.getAddress(),
                caregiver.getUserName());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getId(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getBirthDate(),
                caregiverViewDTO.isGender(),
                caregiverViewDTO.getAddress(),
                caregiverViewDTO.getUserName());
    }
}
