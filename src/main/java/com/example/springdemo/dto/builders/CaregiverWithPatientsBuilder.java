package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.Caregiver;

import java.util.List;
import java.util.stream.Collectors;

public class CaregiverWithPatientsBuilder {
    private CaregiverWithPatientsBuilder(){}

    public static CaregiverWithPatientsDTO generateDTOFromEntity(Caregiver caregiver, List<Patient> patients){
        List<PatientDTO> patientDTOList =  patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new CaregiverWithPatientsDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.isGender(),
                caregiver.getAddress(),
                patientDTOList);
    }

}
