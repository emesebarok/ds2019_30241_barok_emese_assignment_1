package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorBuilder {
    public DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getBirthDate(),
                doctor.isGender(),
                doctor.getAddress(),
                doctor.getUserName());
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO, Integer i){
        String userName;
        if (i == 0) userName = "doctor" + doctorDTO.getUserName();
        else userName = "doctor" + i;
        return new Doctor(
                doctorDTO.getId(),
                doctorDTO.getName(),
                doctorDTO.getBirthDate(),
                doctorDTO.isGender(),
                doctorDTO.getAddress(),
                doctorDTO.getUserName());
    }
}
