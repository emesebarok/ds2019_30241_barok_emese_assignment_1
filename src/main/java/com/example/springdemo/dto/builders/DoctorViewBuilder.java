package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorViewDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorViewBuilder {
    public DoctorViewBuilder() {
    }

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getBirthDate(),
                doctor.isGender(),
                doctor.getAddress(),
                doctor.getUserName());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(
                doctorViewDTO.getId(),
                doctorViewDTO.getName(),
                doctorViewDTO.getBirthDate(),
                doctorViewDTO.isGender(),
                doctorViewDTO.getAddress(),
                doctorViewDTO.getUserName());
    }
}
