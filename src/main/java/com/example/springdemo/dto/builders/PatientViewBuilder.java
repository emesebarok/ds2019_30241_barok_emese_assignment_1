package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.entities.Patient;

public class PatientViewBuilder {
    public PatientViewBuilder() {
    }

    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthDate(),
                patient.isGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getUserName());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientViewDTO){
        return new Patient(
                patientViewDTO.getId(),
                patientViewDTO.getName(),
                patientViewDTO.getBirthDate(),
                patientViewDTO.isGender(),
                patientViewDTO.getAddress(),
                patientViewDTO.getMedicalRecord(),
                patientViewDTO.getUserName());
    }
}
