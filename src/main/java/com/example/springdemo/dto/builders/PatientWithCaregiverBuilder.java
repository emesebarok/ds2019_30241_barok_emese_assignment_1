package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.PatientWithCaregiverDTO;
import com.example.springdemo.entities.Patient;


public class PatientWithCaregiverBuilder {
    public PatientWithCaregiverBuilder() {
    }

    public static PatientWithCaregiverDTO generateDTOFromEntity(Patient patient){

        return new PatientWithCaregiverDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthDate(),
                patient.isGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getUserName(),
                patient.getCaregiver());
    }

    public static Patient generateEntityFromDTO(PatientWithCaregiverDTO patientWithCaregiverDTO, Integer i){
        String userName;
        if (i == 0) userName = "patient" + patientWithCaregiverDTO.getUserName();
        else userName = "patient" + i;
        Patient patient = new Patient(
                patientWithCaregiverDTO.getId(),
                patientWithCaregiverDTO.getName(),
                patientWithCaregiverDTO.getBirthDate(),
                patientWithCaregiverDTO.isGender(),
                patientWithCaregiverDTO.getAddress(),
                patientWithCaregiverDTO.getMedicalRecord(),
                userName);
        patient.setCaregiver(patientWithCaregiverDTO.getCaregiver());
        return patient;
    }
}
