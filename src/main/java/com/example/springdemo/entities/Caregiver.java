package com.example.springdemo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jdk.internal.org.objectweb.asm.tree.InnerClassNode;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregiver")
public class Caregiver extends Person {

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer id, String name, Date birthDate, boolean gender, String address, String userName) {
        super(id, name, birthDate, gender, address, userName);
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
