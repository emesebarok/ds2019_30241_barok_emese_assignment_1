package com.example.springdemo.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "doctor")
public class Doctor extends Person {

    public Doctor() {
        super();
    }

    public Doctor(Integer id, String name, Date birthDate, boolean gender, String address, String userName) {
        super(id, name, birthDate, gender, address, userName);
    }
}
