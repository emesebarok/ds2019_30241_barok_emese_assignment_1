package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "side_effect", length = 300)
    private String sideEffect;

    @Column(name = "dosage", nullable = false)
    private Integer dosage;

//    @OneToMany(mappedBy = "medication", cascade = CascadeType.ALL)
//    private List<TakeThis> takeThisList;

    public Medication(){}

//    public Medication(Integer id, String name, String sideEffect, Integer dosage) {
//        this.id = id;
//        this.name = name;
//        this.sideEffect = sideEffect;
//        this.dosage = dosage;
//    }

    public Medication(Integer id, String name, String sideEffect, Integer dosage) {
        this.id = id;
        this.name = name;
        this.sideEffect = sideEffect;
        this.dosage = dosage;
//        this.takeThisList = takeThisList;
//        this.takeThisList.forEach(x-> x.setMedication(this));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

//    public List<TakeThis> getTakeThisList() {
////        return takeThisList;
//    }
//
//    public void setTakeThisList(List<TakeThis> takeThisList) {
//        this.takeThisList = takeThisList;
//    }
}
