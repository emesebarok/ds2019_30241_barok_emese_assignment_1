package com.example.springdemo.entities;

import javax.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "patient")
public class Patient extends Person{

    @Column(name = "medicalRecord")
    private String medicalRecord;

    @ManyToOne()
    @JoinColumn(name = "caregiver_id", nullable = false)
    //@JsonManagedReference
    private Caregiver caregiver;

    public Patient() {
        super();
    }

    public Patient(Integer id, String name, Date birthDate, boolean gender, String address, String medicalRecord, String userName) {
        super(id, name, birthDate, gender, address, userName);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
