package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    @Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.name")
    List<Caregiver> getAllOrdered();


    @Query(value = "SELECT c.patients " +
            "FROM Caregiver c " +
            "WHERE c.id = ?1"
    )
    List<Patient> getAllFetch(Integer id);

    @Query(value = "SELECT p " +
            "FROM Caregiver p " +
            "WHERE p.userName = ?1"
    )
    Caregiver doesItExist(String userName);
}
