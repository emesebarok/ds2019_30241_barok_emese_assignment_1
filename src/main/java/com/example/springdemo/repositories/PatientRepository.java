package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {
    @Query(value = "SELECT u " +
            "FROM Patient u " +
            "ORDER BY u.name")
    List<Patient> getAllOrdered();

    @Query(value = "SELECT p.caregiver " +
            "FROM Patient p " +
            "WHERE p.id = ?1"
    )
    Caregiver getFetch(Integer id);

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.userName = ?1"
    )
    Patient doesItExist(String userName);
}
