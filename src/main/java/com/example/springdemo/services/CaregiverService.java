package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.CaregiverViewBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverViewDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientDTO> findAllFetch(Integer id){
        List<Patient> patientList = caregiverRepository.getAllFetch(id);

        return patientList.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        List<CaregiverViewDTO> caregivers = findAll();
        Integer i = caregivers.get(caregivers.size() - 1).getId() + 1;

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO, i))
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "user id", caregiverDTO.getId().toString());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO, 0)).getId();
    }

    public Caregiver itExists(String userName) {
        return caregiverRepository.doesItExist(userName);
    }

    public void delete(CaregiverViewDTO caregiverViewDTO){
        caregiverRepository.deleteById(caregiverViewDTO.getId());
    }

}
