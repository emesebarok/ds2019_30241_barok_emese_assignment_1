package com.example.springdemo.services;

import com.example.springdemo.dto.*;
import com.example.springdemo.dto.builders.*;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.validators.MedicationFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationViewDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "med id", id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationViewDTO> findAll(){
        List<Medication> medicationList = medicationRepository.getAllOrdered();

        return medicationList.stream()
                .map(MedicationViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {

        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication", "med id", medicationDTO.getId().toString());
        }
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(MedicationViewDTO medicationViewDTO){
        medicationRepository.deleteById(medicationViewDTO.getId());
    }

}
