package com.example.springdemo.services;

import com.example.springdemo.dto.*;
import com.example.springdemo.dto.builders.*;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private PatientRepository patientRepository;

    @Autowired
    public void setPatientRepository(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientViewDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientViewDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Caregiver findFetch(Integer id){
        return patientRepository.getFetch(id);
    }

    public Integer insert(PatientWithCaregiverDTO patientWithCaregiverDTO) {
        PatientFieldValidator.validateInsertOrUpdate(patientWithCaregiverDTO);

        List<PatientViewDTO> patientViewDTOS = findAll();
        Integer i = patientViewDTOS.get(patientViewDTOS.size() - 1).getId() + 1;

        return patientRepository
                .save(PatientWithCaregiverBuilder.generateEntityFromDTO(patientWithCaregiverDTO, i))
                .getId();
    }

    public Integer update(PatientWithCaregiverDTO patientWithCaregiverDTO) {

        Optional<Patient> patient = patientRepository.findById(patientWithCaregiverDTO.getId());

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "user id", patientWithCaregiverDTO.getId().toString());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientWithCaregiverDTO);

        return patientRepository.save(PatientWithCaregiverBuilder.generateEntityFromDTO(patientWithCaregiverDTO, 0)).getId();
    }

    public Patient itExists(String userName) {
        return patientRepository.doesItExist(userName);
    }

    public void delete(PatientViewDTO patientViewDTO){
        this.patientRepository.deleteById(patientViewDTO.getId());
    }
}
