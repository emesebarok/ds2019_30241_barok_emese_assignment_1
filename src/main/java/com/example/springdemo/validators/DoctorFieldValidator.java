package com.example.springdemo.validators;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class DoctorFieldValidator {
    public DoctorFieldValidator() {
    }

    public static void validateInsertOrUpdate(DoctorDTO doctorDTO) {
        List<String> errors = new ArrayList<>();
        if (doctorDTO == null) {
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }
    }
}
