package com.example.springdemo.validators;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class MedicationFieldValidator {
    public MedicationFieldValidator() {
    }

    public static void validateInsertOrUpdate(MedicationDTO medicationDTO) {

        List<String> errors = new ArrayList<>();
        if (medicationDTO == null) {
            errors.add("medicationDTO is null");
            throw new IncorrectParameterException(MedicationDTO.class.getSimpleName(), errors);
        }
    }
}
