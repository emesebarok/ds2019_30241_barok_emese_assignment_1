package com.example.springdemo.validators;

import com.example.springdemo.dto.PatientWithCaregiverDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PatientFieldValidator {
    public PatientFieldValidator() {
    }

    public static void validateInsertOrUpdate(PatientWithCaregiverDTO patientWithCaregiverDTO) {
        List<String> errors = new ArrayList<>();
        if (patientWithCaregiverDTO == null) {
            errors.add("patientWithCaregiverDTO is null");
            throw new IncorrectParameterException(PatientWithCaregiverDTO.class.getSimpleName(), errors);
        }
    }
}
