package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CaregiverServiceTest extends SpringDemoApplicationTests {

    @Autowired
    CaregiverService caregiverService;


    @Test(expected = IncorrectParameterException.class)
    public void insertDTOBad() {
        CaregiverDTO caregiverDTO = new CaregiverDTO();
        caregiverService.insert(caregiverDTO);

    }
    @Test
    public void insertDTOGood() {
        CaregiverDTO caregiverDTO = new CaregiverDTO();
        caregiverDTO.setName("John Patterson");
        caregiverDTO.setAddress("George Baritiu nr. 22");
        Integer id = caregiverService.insert(caregiverDTO);
        CaregiverViewDTO person2 = caregiverService.findCaregiverById(id);
        assert(!caregiverDTO.equals(person2));

    }
}
